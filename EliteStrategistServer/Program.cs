﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using static EliteStrategistServer.Constants;
using System.Linq;
using Newtonsoft.Json;

namespace EliteStrategistServer
{
	static class Program
	{
		public static Dictionary<TcpClient, NetworkStream> users = new Dictionary<TcpClient, NetworkStream>();
		public static List<int> idInUse;

		static void Main(string[] args)
		{
			TcpListener server = new TcpListener(IPAddress.Any, 9999);
			// we set our IP address as server's address, and we also set the port: 9999

			server.Start();  // this will start the server
			Console.WriteLine("Server is listening.");

			while (true)   //we wait for a connection
			{
				TcpClient client = server.AcceptTcpClient();  //if a connection exists, the server will accept it
				Console.WriteLine("Connected.");
				Thread thread = new Thread(ClientConnection);
				thread.Start(client);
			}
		}

		public static void ClientConnection(object obj)
		{
			var client = (TcpClient)obj;

			NetworkStream ns = client.GetStream(); //networkstream is used to send/receive messages

			users[client] = ns;

			while (client.Connected)  //while the client is connected, we look for incoming messages
			{
				try
				{
					var (code, message) = ns.GetMessage();
					Console.WriteLine("code: " + code + " message: " + message);
					Action(code, message, client);
				}
				catch
				{
					break;
				}
			}
		}

		public static MessageType GetCode(this NetworkStream ns)
		{
			var msg = new byte[sizeof(int)];
			ns.Read(msg, 0, msg.Length);
			return (MessageType)BitConverter.ToInt32(msg, 0);
		}

		public static string GetInfo(this NetworkStream ns)
		{
			var blist = new List<byte>();
			while (!blist.Any() || blist[blist.Count - 1] != 4)
			{
				byte[] msg = new byte[1024];
				int readed;
				do
				{
					readed = ns.Read(msg, 0, msg.Length);
					blist.AddRange(msg.Take(readed));
				} while (readed >= 1024);
			}
			blist.RemoveAt(blist.Count - 1);
			return Encoding.UTF8.GetString(blist.ToArray()).Trim();
		}

		public static (MessageType, string) GetMessage(this NetworkStream ns)
		{
			return (ns.GetCode(), ns.GetInfo());
		}

		public static void Action(MessageType code, string info, TcpClient client)
		{
			switch (code)
			{
				case MessageType.BeginGame:
					dynamic json = JsonConvert.DeserializeObject(info);
					int id = json.Id;
					if (idInUse.Contains(id))
					{
						var response = JsonConvert.SerializeObject(new { Status = false });
						SendToMe(code, response, client);
					}
					else
					{
						idInUse.Add(id);
						var response = JsonConvert.SerializeObject(new { Status = true });
						SendToMe(code, response, client);
						SendToAll(code, info, client);
					}
					break;
				case MessageType.DoMove:
					SendToAll(code, info, client);
					break;
				default:
					break;
			}
		}

		public static void SendToAll(MessageType code, string message, TcpClient client)
		{
			var messList = new List<byte>();
			messList.AddRange(BitConverter.GetBytes((int)(code)));
			messList.AddRange(Encoding.Default.GetBytes(message));
			messList.Add(4);

			byte[] mess = messList.ToArray();
			foreach (var user in users)
			{
				try
				{
					if (user.Key.Connected && user.Key.Client != client.Client)
					{
						user.Value.Write(mess, 0, mess.Length);
					}
				}
				catch
				{

				}
			}
		}

		public static void SendToMe(MessageType code, string message, TcpClient client)
		{
			var messList = new List<byte>();
			messList.AddRange(BitConverter.GetBytes((int)(code)));
			messList.AddRange(Encoding.Default.GetBytes(message));
			messList.Add(4);

			byte[] mess = messList.ToArray();
			foreach (var user in users)
			{
				try
				{
					if (user.Key.Connected && user.Key.Client == client.Client)
					{
						user.Value.Write(mess, 0, mess.Length);
					}
				}
				catch
				{

				}
			}
		}
	}
}
